import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:movie_search/routes/app_pages.dart';
import 'package:movie_search/values/app_colors.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:movie_search/views/home_page_component.dart';

void main() {
  runApp(GetMaterialApp(
    title: 'Movie Search',
    theme: ThemeData(
      primarySwatch: Colors.blue,
      fontFamily: 'Montserrat',
      visualDensity: VisualDensity.adaptivePlatformDensity,
    ),
    supportedLocales: [const Locale('pt', 'BR')],
    locale: Locale('pt', 'BR'),
    getPages: AppPages.routes,
    localizationsDelegates: [
      GlobalWidgetsLocalizations.delegate,
      GlobalMaterialLocalizations.delegate,
    ],
    initialRoute: '/',
    home: HomePageComponent(),
  ));
}
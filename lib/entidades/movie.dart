import 'package:movie_search/entidades/genre.dart';
import 'package:movie_search/values/values.dart';

class Movie {

  int id;
  String title;
  String originalTitle;
  String overview;
  String posterPath;
  String generos;
  double vote_average;
  DateTime release_date;
  double runtime;
  double budget;
  String production_companies;

  Movie({
    this.title,
    this.generos,
    this.id,
    this.originalTitle,
    this.overview,
    this.posterPath,
    this.budget,
    this.release_date,
    this.runtime,
    this.vote_average,
    this.production_companies,
  });

  factory Movie.fromJson(Map<String, dynamic> json) {

    String genresAux = '';
    if(json['genre_ids'] != null) {
      for (int i = 0; i < json['genre_ids'].length; i++) {
        var index = Values.genres.indexWhere((genre) => genre.id == json['genre_ids'][i]);
        if(i != 0){
          genresAux += ' - ';
        }
        if (index != -1) {
          genresAux += Values.genres[index].descricao;
        } else {
          genresAux += ' - Outros';
        }
      }
    }

    return Movie(
      id: json['id'],
      title: json['title'],
      originalTitle: json['original_title'],
      overview: json['overview'],
      posterPath: json['poster_path'],
      generos: genresAux,
      budget: (json['budget'] != null ? double.parse(json['budget'].toString()) : 0.0),
      release_date: (json['release_date'] != null ? DateTime.parse(json['release_date'].toString()) : null),
      runtime: (json['runtime'] != null ? double.parse(json['runtime'].toString()) : 0.0),
      vote_average: (json['vote_average'] != null ? double.parse(json['vote_average'].toString()) : 0.0),
      production_companies: (
          json['production_companies'] != null &&
              json['production_companies'].length > 0 ?
          json['production_companies'][0]['name'] : 'Sem Informações'
      ),
    );
  }

}
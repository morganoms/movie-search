class Genre {
  int id;
  String descricao;
  bool selected;

  Genre({this.id, this.descricao, this.selected});

  factory Genre.fromJson(Map<String, dynamic> json) {
    return Genre(
      id: json['id'],
      descricao: json['name'],
      selected: false,
    );
  }
}
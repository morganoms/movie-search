import 'package:get/get.dart';
import 'package:movie_search/controllers/movie_detail_controller.dart';

class MovieDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MovieDetailController>(() {
      return MovieDetailController();
    });
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:movie_search/controllers/movie_detail_controller.dart';
import 'package:movie_search/entidades/movie.dart';
import 'package:movie_search/values/app_colors.dart';
import 'package:intl/intl.dart';
import 'package:movie_search/values/values.dart';

class MovieDetailComponent extends GetView<MovieDetailController> {
  final MovieDetailController movieController = Get.put(MovieDetailController());
  final oCcy = new NumberFormat("#,##0.00", "en_US");

  @override
  Widget build(BuildContext context) {
    List args = Get.arguments;
    movieController.movie.value = args[0];
    movieController.castDirectors.assignAll(args[1]);
   return WillPopScope(
       child: Scaffold(
         body: SingleChildScrollView(
           child: Container(
             padding: EdgeInsets.only(bottom: 56),
             child: GetX<MovieDetailController>(
               init: MovieDetailController(),
               builder: (_){
                 return Column(
                   children: [
                     Container(
                       height: 450,
                       child: Stack(
                         fit: StackFit.loose,
                         alignment: Alignment.topCenter,
                         children: [
                           Container(
                             height: 300,
                             color: Colors.grey[200],
                           ),
                           Container(
                             alignment: Alignment.topLeft,
                             padding: EdgeInsets.only(left: 20, top: 56),
                             child: RaisedButton(
                                 color: Colors.white,
                                 shape: RoundedRectangleBorder(
                                     borderRadius: BorderRadius.circular(18.0),
                                     side: BorderSide(color: Colors.white)
                                 ),
                                 child: Row(
                                   mainAxisSize: MainAxisSize.min,
                                   children: [
                                     Icon(
                                       Icons.arrow_back_ios,
                                       color: Colors.grey[700],
                                     ),
                                     Text(
                                       "Voltar",
                                       style: TextStyle(
                                           color: Colors.grey[700],
                                           fontSize: 18
                                       ),
                                     ),
                                   ],
                                 ),
                                 onPressed: (){
                                   _.onBackClick();
                                 }
                             ),
                           ),
                           Container(
                             padding: EdgeInsets.only(top: 56),
                             alignment: Alignment.bottomCenter,
                             child: ClipRRect(
                               borderRadius: BorderRadius.circular(10.0),
                               child: FadeInImage.assetNetwork(
                                 image: Values.URL_IMAGENS + _.movie.value.posterPath,
                                 placeholder: 'lib/imagens/loadingPoster.webp',
                                 height: 300,
                               ),
                             ),
                           ),
                         ],
                       ),
                     ),
                     Container(
                       padding: EdgeInsets.only(top: 36),
                       child: Row(
                         crossAxisAlignment: CrossAxisAlignment.end,
                         mainAxisSize: MainAxisSize.min,
                         children: [
                           Text(
                             Get.find<MovieDetailController>().movie.value.vote_average.toString(),
                             style: TextStyle(
                                 fontSize: 34,
                                 color: AppColors.corPrincipal,
                                 fontWeight: FontWeight.w700
                             ),
                           ),
                           Text(
                             ' / 10',
                             style: TextStyle(
                               fontSize: 24,
                               color: Colors.grey,
                               // fontWeight: FontWeight.w700
                             ),
                           ),
                         ],
                       ),
                     ),
                     Container(
                       padding: EdgeInsets.only(top: 32),
                       child: Text(
                         Get.find<MovieDetailController>().movie.value.title.toUpperCase(),
                         style: TextStyle(
                             fontSize: 20,
                             color: AppColors.corPrincipal,
                             fontWeight: FontWeight.w700
                         ),
                       ),
                     ),
                     Container(
                       padding: EdgeInsets.only(top: 12),
                       child: Text(
                         "Título original: " + Get.find<MovieDetailController>().movie.value.originalTitle,
                         style: TextStyle(
                           fontSize: 14,
                         ),
                       ),
                     ),
                     Container(
                       padding: EdgeInsets.only(top: 40),
                       child: Row(
                         crossAxisAlignment: CrossAxisAlignment.end,
                         mainAxisSize: MainAxisSize.min,
                         children: [
                           Padding(
                             padding: EdgeInsets.only(right: 12),
                             child: Container(
                               padding: EdgeInsets.all(16),
                               decoration: BoxDecoration(
                                 borderRadius: BorderRadius.circular(7),
                                 color: Colors.grey[200],
                               ),
                               child: Row(
                                 crossAxisAlignment: CrossAxisAlignment.end,
                                 mainAxisSize: MainAxisSize.min,
                                 children: [
                                   Text(
                                     "Ano: ",
                                     style: TextStyle(
                                         fontSize: 18,
                                         color: Colors.grey[400],
                                         fontWeight: FontWeight.w700
                                     ),
                                   ),
                                   Text(
                                     Get.find<MovieDetailController>().movie.value.release_date.year.toString(),
                                     style: TextStyle(
                                         fontSize: 20,
                                         color: AppColors.corPrincipal,
                                         fontWeight: FontWeight.w700
                                       // fontWeight: FontWeight.w700
                                     ),
                                   ),
                                 ],
                               ),
                             ),
                           ),
                           Container(
                               decoration: BoxDecoration(
                                 borderRadius: BorderRadius.circular(7),
                                 color: Colors.grey[200],
                               ),
                               child: Container(
                                 padding: EdgeInsets.all(16),
                                 child: Row(
                                   crossAxisAlignment: CrossAxisAlignment.end,
                                   mainAxisSize: MainAxisSize.min,
                                   children: [
                                     Text(
                                       "Duração: ",
                                       style: TextStyle(
                                           fontSize: 18,
                                           color: Colors.grey[400],
                                           fontWeight: FontWeight.w700
                                       ),
                                     ),
                                     Text(
                                       (Get.find<MovieDetailController>().movie.value.runtime ~/ 60).toStringAsFixed(0) + "h " +
                                           (Get.find<MovieDetailController>().movie.value.runtime % 60).toStringAsFixed(0) + " min",
                                       style: TextStyle(
                                           fontSize: 20,
                                           color: AppColors.corPrincipal,
                                           fontWeight: FontWeight.w700
                                         // fontWeight: FontWeight.w700
                                       ),
                                     ),
                                   ],
                                 ),
                               )
                           ),
                         ],
                       ),
                     ),
                     Container(
                       padding: EdgeInsets.only(top: 24),
                       child: Text(
                           Get.find<MovieDetailController>().movie.value.generos,
                         style: TextStyle(
                           color: AppColors.corPrincipal
                         ),
                       ),
                     ),
                     Container(
                       alignment: Alignment.topLeft,
                       padding: EdgeInsets.only(left: 20, right: 20, top: 56),
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: [
                           Text(
                             "Descrição",
                             style: TextStyle(
                               fontSize: 18,
                               color: Colors.grey,
                               // fontWeight: FontWeight.w700
                             ),
                             textAlign: TextAlign.start,
                           ),
                           Container(
                             padding: EdgeInsets.only(top: 8),
                             child: Text(
                                 Get.find<MovieDetailController>().movie.value.overview,
                               style: TextStyle(
                                 color: AppColors.corPrincipal,
                                 fontWeight: FontWeight.w700
                               ),
                             ),
                           )
                         ],
                       ),
                     ),
                     Container(
                       padding: EdgeInsets.only(top: 40, left: 20, right: 20),
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.center,
                         mainAxisSize: MainAxisSize.max,
                         children: [
                           Padding(
                             padding: EdgeInsets.only(bottom: 12),
                             child: Container(
                               width: double.maxFinite,
                               padding: EdgeInsets.all(16),
                               decoration: BoxDecoration(
                                 borderRadius: BorderRadius.circular(7),
                                 color: Colors.grey[200],
                               ),
                               child: Row(
                                 crossAxisAlignment: CrossAxisAlignment.end,
                                 mainAxisSize: MainAxisSize.min,
                                 children: [
                                   Text(
                                     "ORÇAMENTO: ",
                                     style: TextStyle(
                                         fontSize: 14,
                                         color: Colors.grey[400],
                                         fontWeight: FontWeight.w700
                                     ),
                                   ),
                                   FittedBox(
                                     fit: BoxFit.fitWidth,
                                     child: Text(
                                       "\$ "+ this.oCcy.format( Get.find<MovieDetailController>().movie.value.budget),
                                       style: TextStyle(
                                           fontSize: 14,
                                           color: AppColors.corPrincipal,
                                           fontWeight: FontWeight.w700
                                       ),
                                     ),
                                   ),
                                 ],
                               ),
                             ),
                           ),
                           Container(
                               width: double.maxFinite,
                               decoration: BoxDecoration(
                                 borderRadius: BorderRadius.circular(7),
                                 color: Colors.grey[200],
                               ),
                               child: Container(
                                 padding: EdgeInsets.all(16),
                                 child: Row(
                                   crossAxisAlignment: CrossAxisAlignment.end,
                                   mainAxisSize: MainAxisSize.min,
                                   children: [
                                     Text(
                                       "PRODUTORAS: ",
                                       style: TextStyle(
                                           fontSize: 14,
                                           color: Colors.grey[400],
                                           fontWeight: FontWeight.w700
                                       ),
                                     ),
                                     FittedBox(
                                       fit: BoxFit.fitWidth,
                                       child: Text(
                                         Get.find<MovieDetailController>().movie.value.production_companies ,
                                         style: TextStyle(
                                             fontSize: 14,
                                             color: AppColors.corPrincipal,
                                             fontWeight: FontWeight.w700
                                         ),
                                       ),
                                     ),
                                   ],
                                 ),
                               )
                           ),
                         ],
                       ),
                     ),
                     Container(
                       alignment: Alignment.topLeft,
                       padding: EdgeInsets.only(left: 20, right: 20, top: 56),
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: [
                           Text(
                             "Diretor(s)",
                             style: TextStyle(
                               fontSize: 18,
                               color: Colors.grey,
                               // fontWeight: FontWeight.w700
                             ),
                             textAlign: TextAlign.start,
                           ),
                           Container(
                             padding: EdgeInsets.only(top: 8),
                             child: Text(
                               Get.find<MovieDetailController>().castDirectors[1],
                               style: TextStyle(
                                   color: AppColors.corPrincipal,
                                   fontWeight: FontWeight.w700
                               ),
                             ),
                           )
                         ],
                       ),
                     ),
                     Container(
                       alignment: Alignment.topLeft,
                       padding: EdgeInsets.only(left: 20, right: 20, top: 56),
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: [
                           Text(
                             "Elenco",
                             style: TextStyle(
                               fontSize: 18,
                               color: Colors.grey,
                               // fontWeight: FontWeight.w700
                             ),
                             textAlign: TextAlign.start,
                           ),
                           Container(
                             padding: EdgeInsets.only(top: 8),
                             child: Text(
                               Get.find<MovieDetailController>().castDirectors[0],
                               style: TextStyle(
                                   color: AppColors.corPrincipal,
                                   fontWeight: FontWeight.w700
                               ),
                             ),
                           )
                         ],
                       ),
                     ),
                   ],
                 );
               },
             ),
           ),
         ),
       ),
   );
  }

}
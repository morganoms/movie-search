import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:get/get.dart';
import 'package:movie_search/controllers/home_page_controller.dart';
import 'package:movie_search/entidades/movie.dart';
import 'package:movie_search/values/app_colors.dart';
import 'package:movie_search/values/values.dart';

class HomePageComponent extends GetView<HomePageController> {

  final HomePageController _homePage = Get.put(HomePageController());

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: Scaffold(
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Container(
              padding: EdgeInsets.only(top: 56, left: 20, right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    width: double.maxFinite,
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(bottom: 24),
                    child: Text(
                      "Filmes",
                      style: TextStyle(
                        color: AppColors.corPrincipal,
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  GetX<HomePageController>(
                    init: HomePageController(),
                    builder: (_){
                      return Padding(
                        padding: EdgeInsets.only(bottom: 16),
                        child: Container(
                          width: double.maxFinite,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.grey[200],
                          ),
                          child: TextField(
                            controller: _.searchController.value,
                            style: TextStyle(
                              fontSize: 18.0,
                              color:AppColors.corPrincipal,
                            ),
                            onSubmitted: (String str){
                              _.serviceSearchMovie(str);
                            },
                            decoration: InputDecoration(
                              labelText: 'Pesquisar filmes',
                              prefixIcon: Icon(Icons.search),
                              suffix: GestureDetector(
                                onTap: (){
                                  _.isSearching.value = false;
                                  _.searchController.value.clear();
                                  FocusScopeNode currentFocus = FocusScope.of(context);

                                  if (!currentFocus.hasPrimaryFocus &&
                                      currentFocus.focusedChild != null) {
                                    FocusManager.instance.primaryFocus.unfocus();
                                  }
                                  // FocusScope.of(context).requestFocus(new FocusNode());
                                  _.selectGenre(0);
                                },
                                behavior: HitTestBehavior.translucent,
                                child: Icon(Icons.close),
                              ),
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                  Container(
                    width: double.maxFinite,
                    child: FittedBox(
                      fit: BoxFit.fitWidth,
                      child: GetBuilder<HomePageController>(
                          builder: (_) {
                            return Row(
                              children: [
                                RaisedButton(
                                  elevation: 0,
                                  color: (_.genersSelected[0].selected ? AppColors.corPrincipal : Colors.white),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(
                                          color: (AppColors.corPrincipal)
                                      )
                                  ),
                                  child: Text(
                                    _.genersSelected[0].descricao,
                                    style: TextStyle(
                                      color: (_.genersSelected[0].selected ? Colors.white : AppColors.corPrincipal),
                                    ),
                                  ),
                                  onPressed: () {
                                    _.selectGenre(0);
                                  },
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 18),
                                  child: RaisedButton(
                                      elevation: 0,
                                      color: (_.genersSelected[1].selected ? AppColors.corPrincipal : Colors.white),
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(18.0),
                                          side: BorderSide(color: AppColors.corPrincipal)
                                      ),
                                      child: Text(
                                        _.genersSelected[1].descricao,
                                        style: TextStyle(
                                          color: (_.genersSelected[1].selected ? Colors.white : AppColors.corPrincipal),
                                        ),
                                      ),
                                      onPressed: (){
                                        _.selectGenre(1);
                                      }
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 18),
                                  child: RaisedButton(
                                      elevation: 0,
                                      color: (_.genersSelected[2].selected ? AppColors.corPrincipal : Colors.white),
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(18.0),
                                          side: BorderSide(color: AppColors.corPrincipal)
                                      ),
                                      child: Text(
                                        _.genersSelected[2].descricao,
                                        style: TextStyle(
                                          color: (_.genersSelected[2].selected ? Colors.white : AppColors.corPrincipal),
                                        ),
                                      ),
                                      onPressed: (){
                                        _.selectGenre(2);
                                      }
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 18),
                                  child: RaisedButton(
                                      elevation: 0,
                                      color: (_.genersSelected[3].selected ? AppColors.corPrincipal : Colors.white),
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(18.0),
                                          side: BorderSide(color: AppColors.corPrincipal)
                                      ),
                                      child: Text(
                                        _.genersSelected[3].descricao,
                                        style: TextStyle(
                                          color: (_.genersSelected[3].selected ? Colors.white : AppColors.corPrincipal),
                                        ),
                                      ),
                                      onPressed: (){
                                        _.selectGenre(3);
                                      }
                                  ),
                                )
                              ],
                            );
                          }),
                    ),
                  ),
                  GetX<HomePageController>(
                    builder: (_){
                      return  Container(
                        child: Expanded(
                          child: CustomScrollView(
                              slivers: <Widget>[
                                SliverPadding(
                                  padding: const EdgeInsets.all(8.0),
                                  sliver: PagewiseSliverList(
                                    pageLoadController: Get.find<HomePageController>().pageLoadController.value,
                                    itemBuilder: this._itemBuilder,
                                  ),
                                ),
                              ]
                          ),
                        ),
                      );
                    },
                  )
                ],
              ),
            ),
          ),
        )
    );
  }



  Widget _itemBuilder(context, Movie movie, _) {
    return GestureDetector(
      onTap: (){
        Get.find<HomePageController>().openMovieDetail(movie.id);
      },
      child: Container(
        child: Column(
          children: [
            Container(
              child: Card(
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.white70, width: 1),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Stack(
                  alignment: Alignment.bottomLeft,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),
                      child: FadeInImage.assetNetwork(
                          image: Values.URL_IMAGENS + movie.posterPath,
                          placeholder: 'lib/imagens/loadingPoster.webp',
                          fit: BoxFit.fitHeight
                      ),
                    ),
                    Container(
                      height: 200,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          gradient: LinearGradient(
                              begin: FractionalOffset.topCenter,
                              end: FractionalOffset.bottomCenter,
                              colors: [
                                Colors.black.withOpacity(0.0),
                                Colors.black,
                              ],
                              stops: [
                                0.0,
                                1.0
                              ]
                          ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 24, bottom: 32),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(bottom: 16),
                            child: Text(
                              movie.title.toUpperCase(),
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                                fontSize: 16
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(bottom: 16),
                            child: Text(
                              movie.generos,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12
                              ),
                              textAlign: TextAlign.left,
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}
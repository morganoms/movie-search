import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:get/get.dart';
import 'package:movie_search/entidades/genre.dart';
import 'package:movie_search/entidades/movie.dart';
import 'package:movie_search/routes/app_pages.dart';
import 'package:movie_search/values/values.dart';
import 'package:movie_search/webservices/services.dart';

class HomePageController extends GetxController {

  final genreId = 28.obs;
  final isSearching = false.obs;
  var searchController = TextEditingController().obs;

  final Services service = new Services();

  final movieList = new List<Movie>().obs;

  final genersSelected = [
    new Genre(
      id: Values.ACAO_ID,
      descricao: "Ação",
      selected: true,
    ),
    new Genre(
      id: Values.AVENTURA_ID,
      descricao: "Aventura",
      selected: false,
    ),
    new Genre(
      id: Values.FANTASIA_ID,
      descricao: "Fantasia",
      selected: false,
    ),new Genre(
      id: Values.COMEDIA_ID,
      descricao: "Comédia",
      selected: false,
    )
  ];

  final pageLoadController = PagewiseLoadController<Movie>(
      pageSize: 20,
      pageFuture: (pageIndex) => Get.find<HomePageController>().serviceGetMovie(pageIndex+1),
  ).obs;

  @override
  void onInit() async{
    await this.getGenres();
  }

  openMovieDetail(int movieId) async{
    var requestMovie = "/3/movie/${movieId.toString()}";
    var requestCast = "/3/movie/${movieId.toString()}/credits";
    Movie result = await this.service.serviceGetMovie(requestMovie);
    List<String> castDirectos = await this.service.serviceCast(requestCast);
    Get.toNamed(Routes.MOVIE_DETAIL, arguments: [result, castDirectos]);
  }

  selectGenre(int index) async {
    for(int i = 0; i < this.genersSelected.length; i++) {
      this.genersSelected[i].selected = false;
    }
    this.genersSelected[index].selected = true;
    this.genreId.value = this.genersSelected[index].id;
    this.pageLoadController.value.reset();
    update();
  }

  getGenres() async{
    try{
      var result = await service.serviceGetGenrer();
      if(result != null){
        Values.refreshGenres(result);
        Values.genres = result;
      }
    } catch(error) {
      print(error.toString());
    }
  }

  void serviceSearchMovie(String name) async {
    for(int i = 0; i < this.genersSelected.length; i++) {
      this.genersSelected[i].selected = false;
    }
    this.isSearching.value = true;
    this.pageLoadController.value.reset();
  }


  Future<List<Movie>> serviceGetMovie(int page) async{
    try{
      Services service = new Services();
      var result = await service.serviceMovieGenre(this.genreId.value, page, isSearching.value, searchController.value.text.trim());
      return result;
    } catch(error) {
      print(error.toString());
    }
  }


}
import 'package:get/get.dart';
import 'package:movie_search/entidades/movie.dart';


class MovieDetailController extends GetxController{
  final movie = new Movie().obs;
  final castDirectors = new List<String>().obs;



  onBackClick() {
    Get.back();
  }
}
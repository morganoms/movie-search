import 'package:movie_search/entidades/genre.dart';

class Values {

  //config API
  static String URL_API = "api.themoviedb.org";
  static String DISCOVER_GENER = "/3/discover/movie/";
  static String GENRE_LIST = "/3/genre/movie/list";
  static String SEARCH_MOVIE = "/3/search/movie";
  static String API_KEY = "0289a624c338b19ee60717ae0bc39ab1";
  static String IDIOMA_API = "pt-BR";
  static String URL_IMAGENS = "https://image.tmdb.org/t/p/original";

  //O valor desta lista é atualizado sempre que a tela
  //de listagem é chamada
  static List<Genre> genres = new List<Genre>();


  //ID Generos Filmes
  static int ACAO_ID = 28;
  static int AVENTURA_ID = 12;
  static int FANTASIA_ID = 14;
  static int COMEDIA_ID = 35;

  static void  refreshGenres(List<Genre> newGenres) {
    Values.genres = newGenres;
  }
}

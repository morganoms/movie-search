import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MaterialAccentCores extends ColorSwatch<int> {

  const MaterialAccentCores(int primary, Map<int, Color> swatch) : super(primary, swatch);
  Color get shade50 => this[50];
  Color get shade100 => this[100];
  Color get shade200 => this[200];
  Color get shade400 => this[400];
  Color get shade700 => this[700];

}

class AppColors {
  AppColors._();

  static const int COR_PRINCIPAL = 0xFF343A40;


  static const MaterialAccentCores corPrincipal = MaterialAccentCores(
    COR_PRINCIPAL,
    <int, Color>{
      100: Color(COR_PRINCIPAL),
      200: Color(COR_PRINCIPAL),
      400: Color(COR_PRINCIPAL),
      700: Color(COR_PRINCIPAL),
    },
  );

}
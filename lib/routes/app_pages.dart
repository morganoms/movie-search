import 'package:get/get.dart';
import 'package:movie_search/bindings/home_page_binding.dart';
import 'package:movie_search/bindings/movie_detail_binding.dart';
import 'package:movie_search/views/home_page_component.dart';
import 'package:movie_search/views/movie_detail_component.dart';
part './app_routes.dart';

class AppPages {

  static final routes = [
    GetPage(name: Routes.HOME, page: () => HomePageComponent(), binding: HomePageBinding()),
    GetPage(name: Routes.MOVIE_DETAIL, page: () => MovieDetailComponent(), binding: MovieDetailBinding()),

  ];
}
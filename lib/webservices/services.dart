import 'dart:convert';

import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:movie_search/entidades/genre.dart';
import 'package:movie_search/entidades/movie.dart';
import 'package:movie_search/values/values.dart';
import 'package:movie_search/webservices/params.dart';
import 'package:movie_search/webservices/webservice.dart';

class Services {
  Future<List<String>> serviceCast(String request) async {
    try{
      Map params = await Params.paramsGetMovie();
      return Webservice.serviceGet(request, params: params).then((value) {
        var jsonDecodificado = json.decode(value.body);
        if(value.statusCode == 200){

          String cast = "";
          if(jsonDecodificado['cast'] != null) {
            for (int i = 0; i < jsonDecodificado['cast'].length; i++) {
              if(i != 0){
                cast += ', ';
              }
              cast += jsonDecodificado['cast'][i]['name'];
            }
          }

          String directors = "";
          if(jsonDecodificado['crew'] != null) {
            for (int i = 0; i < jsonDecodificado['crew'].length; i++) {
              if(jsonDecodificado['crew'][i]['job'] == "Director"){
                if(directors != ""){
                  directors += ', ';
                }
                directors += jsonDecodificado['crew'][i]['name'];
              }
            }
          }
          return [cast, directors];
        }
        else{
          throw Exception(jsonDecodificado['message']);
        }
      });
    }
    catch(e){
      throw Exception(e.toString());
    }

  }
  Future<List<Movie>> serviceMovieGenre(int genreId, int page, bool isSearching, String query) async{
    Map params;
    String request;
    try{
      if(isSearching){
        params = await Params.paramsSearchMovie(query, page);
        request = Values.SEARCH_MOVIE;
      } else {
        params = await Params.paramsDiscoverGenrer(genreId, page);
        request = Values.DISCOVER_GENER;
      }

      return Webservice.serviceGet(request, params: params).then((value) {
        List<Movie> retorno = new List<Movie>();
        var jsonDecodificado = json.decode(value.body);

        if(value.statusCode == 200){

          var items = (jsonDecodificado['results'] as List).map((i) => new Movie.fromJson(i));
          for (var item in items) {
            Movie movie = new Movie(
              id: item.id,
              posterPath: item.posterPath,
              overview: item.overview,
              originalTitle: item.originalTitle,
              title: item.title,
              generos: item.generos,
              vote_average: item.vote_average,
              runtime: item.runtime,
              release_date: item.release_date,
              budget: item.budget
            );
            retorno.add(movie);
          }
          return retorno;
        }
        else{
          throw Exception(jsonDecodificado['message']);
        }
      });
    }
    catch(e){
      throw Exception(e.toString());
    }
  }


  Future<List<Genre>> serviceGetGenrer() async {
    try {
      Map params = await Params.paramsGetGenrer();
      return Webservice.serviceGet(Values.GENRE_LIST, params: params).then((
          value) {
        List<Genre> retorno = new List<Genre>();
        var jsonDecodificado = json.decode(value.body);
        if (value.statusCode == 200) {
          var items = (jsonDecodificado['genres'] as List).map((i) =>
          new Genre.fromJson(i));
          for (var item in items) {
            Genre genre = new Genre(
                id: item.id,
                descricao: item.descricao,
                selected: item.selected
            );
            retorno.add(genre);
          }
          return retorno;
        }
        else {
          throw Exception(jsonDecodificado['message']);
        }
      });
    }
    catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<Movie> serviceGetMovie(String request) async {
    try{
      Map params = await Params.paramsGetMovie();
      return Webservice.serviceGet(request, params: params).then((value) {
        var jsonDecodificado = json.decode(value.body);
        if(value.statusCode == 200){

          String genresAux = '';
          if(jsonDecodificado['genres'] != null) {
            for (int i = 0; i < jsonDecodificado['genres'].length; i++) {
              var index = Values.genres.indexWhere((genre) => genre.id == jsonDecodificado['genres'][i]['id']);
              if(i != 0){
                genresAux += ' - ';
              }
              if (index != -1) {
                genresAux += Values.genres[index].descricao;
              } else {
                genresAux += ' - Outros';
              }
            }
          }
          print(jsonDecodificado['production_companies'].length);
          Movie movie = new Movie(
              id: jsonDecodificado['id'],
              title: jsonDecodificado['title'],
              originalTitle: jsonDecodificado['original_title'],
              overview: jsonDecodificado['overview'],
              posterPath: jsonDecodificado['poster_path'],
              generos: genresAux,
              budget: (jsonDecodificado['budget'] != null ? double.parse(jsonDecodificado['budget'].toString()) : 0.0),
              release_date: (jsonDecodificado['release_date'] != null ? DateTime.parse(jsonDecodificado['release_date'].toString()) : null),
              runtime: (jsonDecodificado['runtime'] != null ? double.parse(jsonDecodificado['runtime'].toString()) : 0.0),
              vote_average: (jsonDecodificado['vote_average'] != null ? double.parse(jsonDecodificado['vote_average'].toString()) : 0.0),
              production_companies: (
                  jsonDecodificado['production_companies'] != null &&
                  jsonDecodificado['production_companies'].length > 0 ?
                  jsonDecodificado['production_companies'][0]['name'] : 'Sem Informações'),
          );
          return movie;
        }
        else{
          throw Exception(jsonDecodificado['message']);
        }
      });
    }
    catch(e){
      throw Exception(e.toString());
    }
  }
}
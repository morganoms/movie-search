import 'package:movie_search/values/values.dart';

class Params {
  static Future<Map<String, String>> paramsDiscoverGenrer(int idGenero, int page) async{
    Map<String, String> params = {
      "api_key": Values.API_KEY,
      "language": Values.IDIOMA_API,
      "page": page.toString(),
      "with_genres": idGenero.toString(),
    };
    return params;
  }

  static Future<Map<String, String>> paramsGetGenrer() async{
    Map<String, String> params = {
      "api_key": Values.API_KEY,
      "language": Values.IDIOMA_API,
    };
    return params;
  }

  static Future<Map<String, String>> paramsGetMovie() async{
    Map<String, String> params = {
      "api_key": Values.API_KEY,
      "language": Values.IDIOMA_API,
    };
    return params;
  }

  static Future<Map<String, String>> paramsSearchMovie(String query, int page) async{
    Map<String, String> params = {
      "api_key": Values.API_KEY,
      "language": Values.IDIOMA_API,
      "page": page.toString(),
      "query": query,
    };
    return params;
  }
}
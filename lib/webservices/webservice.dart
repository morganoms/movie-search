import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:movie_search/exceptions/sem_conexao.dart';
import 'package:movie_search/values/values.dart';

class Webservice {
  static Future<http.Response> serviceGet(String tipoRota, {params}) async {
    try {
      var uri_url = Uri.http(Values.URL_API, tipoRota, params);
      var header = {"Content-Type": "application/json"};
      var response = await http
          .get(uri_url, headers: header)
          .timeout(Duration(seconds: 25));
      return response;
    } on SocketException {
      throw SemConexaoException();
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}